import React, { useState } from "react";
// import './App.css';
import { AppBar } from "@material-ui/core";
import AreaList from "./components/AreaList";
import AreaSelect from "./components/AreaSelect";
import ProgressStatus from "./components/ProgressStatus"
import Notification from "./components/Notification"
import Cookies from "js-cookie";
import {getCurrentArea} from "./utility"

export const AppBarContext = React.createContext({});
function App() {
  const [customContext, setCustomContext] = useState({
    ...getCurrentArea(Cookies.get("areaId")), ...{progressFlag: true, notification: {open: false, messageType: ''}},
  });
  
  return (
    <AppBarContext.Provider value={customContext}>
        <AppBar position="static">
          <AreaSelect
        　  setCustomContext={setCustomContext}
          />
        </AppBar>
        <ProgressStatus customContext={customContext} />
        <Notification customContext={customContext} setCustomContext={setCustomContext} />
        <AreaList
        customContext={customContext}
        setCustomContext={setCustomContext}
        />
        
    </AppBarContext.Provider>
  )
};

export default App;
