import config from "../config";
import Cookies from "js-cookie";
import { AreaProps, LatLonProps } from "../components/types"

export function getCurrentArea(areaId: string|undefined): AreaProps {
  let latLon: LatLonProps|undefined;
  if (areaId){
    latLon = config.latLonList[areaId];
  }
  if (!areaId || !latLon) {
    areaId = "hiroshima";
    Cookies.set("areaId", areaId);
  }

  return {areaId};
}