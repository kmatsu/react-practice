import { LatLonListProps } from "../components/types"

// TODO: city list https://restaurant-api.wolt.com/v1/cities

interface configInterface {
    notificationMessages: {[key: string]: string};
    latLonList: LatLonListProps;
    
};
const config: configInterface = {
    notificationMessages: {apiError: 'API connection Error'},
    latLonList: {"hiroshima": {lat: 34.394924212975, lon: 132.46209807622483},
                 "sapporo": {lat: 43.062444665980735, lon: 141.35662963544843},
                 "almaty": {lat: 43.245132, lon: 76.954158},
                 "nur-sultan": {lat: 51.145319, lon: 71.412776
                },
    }
};

export default config;
