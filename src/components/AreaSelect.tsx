import React, {useEffect} from "react";
import config from "../config";
import {
    Select,
    MenuItem
  } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import Cookies from "js-cookie";
import {getCurrentArea} from "../utility"


const AreaSelect = ({setCustomContext}: any) => {
    const [currentAreaId, setAreaId] = React.useState(
        getCurrentArea(Cookies.get("areaId")).areaId,
    );
    const handleAreaId = (event: React.ChangeEvent<{}>) => {
        const { value } = event.target as HTMLInputElement;
        Cookies.set("areaId",  value);
        setAreaId(value);
    };
    useEffect(() => {
        setCustomContext((prev: any) => ({
          ...prev,
          areaId: currentAreaId,
        }));
      }, [currentAreaId, setCustomContext]);
    return(
        <FormControl>
          <Select
            value={currentAreaId}
            onChange={handleAreaId}>
            {Object.keys(config.latLonList).map((key, index) => (
                <MenuItem value={key} key={index}>{key}</MenuItem>
            
            ))}           
          </Select>
        </FormControl>
    )
};

export default AreaSelect;