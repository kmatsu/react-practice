import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import config from "../config"

const Notification = ({customContext, setCustomContext}: any) => {
    let message: string = '';
    if (customContext.notification.messageType) {
        message = config.notificationMessages[customContext.notification.messageType];
    }
    const handleClose = () => {
        setCustomContext((prev: any) => ({
            ...prev,
            notification: {open: false, messageType: ''},
          }));
    };
    //customContext
    return (
        <>
            <Snackbar
                open={customContext.notification.open}
                autoHideDuration={6000}
                onClose={handleClose}
                message={message}
            />
        </>
    )
};

export default Notification;