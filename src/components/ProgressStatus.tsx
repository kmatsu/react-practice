import React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";


const ProgressStatus = ({customContext}: any) => {

    return (
        <>
        {customContext.progressFlag && (
            <LinearProgress />
        )}
        </>
    )
};

export default ProgressStatus;