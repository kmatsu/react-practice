export interface AreaProps { areaId: string; }
export interface LatLonProps {lat: number, lon: number }
export interface LatLonListProps {[key: string]: LatLonProps}
export interface LangResultProps {lang: string, value: string}
export interface AreaResultProps {name: [LangResultProps]}
