import React, {useEffect, useState} from "react";
import axios from "axios";
import config from "../config";
import {AreaResultProps, LangResultProps} from "./types"

const AreaList = ({customContext, setCustomContext}: any) => {
    const { areaId } = customContext;
    const [areaData, setAreaData] = useState<AreaResultProps[]>([]); 
    useEffect(()=>{
        // TODO: logic will be changed
        const url = "/v3/venues/delivers-to?lat=" + config.latLonList[areaId].lat + "&lon=" + config.latLonList[areaId].lon;
        const axiosConfig = {
            mode: 'no-cors',
            headers: {
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
            },
            withCredentials: true,
            credentials: 'same-origin',
            crossdomain: true
        };
        
        async function loadData(url: string) {
            try {
                const response = await axios.get(url, axiosConfig);
                let tempAreaData = [];
                for (const result of response.data.results) {
                    tempAreaData.push(result);
                    setAreaData({...result});
                }
                setAreaData(tempAreaData);
            } catch (error) {
                console.error(error);
                setCustomContext((prev: any) => ({
                    ...prev,
                    notification: {open: true, messageType: 'apiError'},
                  }));
            } finally {
                setProgressFlag(false);
            }
        }
        const setProgressFlag = (progressFlag: boolean) => {
            setCustomContext((prev: any) => ({
              ...prev,
              progressFlag: progressFlag,
            }));
        };
        setProgressFlag(true);
        loadData(url);
    }, [areaId, setCustomContext]);
    return (
        <>
        {areaData.length > 0 && areaData.map((result, index) => {
            const shopNameBase: LangResultProps|undefined = result.name.find(e => e.lang === "en");
            if (shopNameBase){
                return (
                   <div key={index}>{shopNameBase.value}</div>
                )
            }
            
        })}
        </>
    );
};

export default AreaList;